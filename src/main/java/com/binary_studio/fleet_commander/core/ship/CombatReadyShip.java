package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private final String name;

	private final PositiveInteger shieldHP;

	private PositiveInteger currentShieldHP;

	private final PositiveInteger hullHP;

	private PositiveInteger currentHullHP;

	private final PositiveInteger maxCapacitorAmount;

	private PositiveInteger capacitorAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private final AttackSubsystem attackSubsystem;

	private final DefenciveSubsystem defenciveSubsystem;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size, AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
		this.currentShieldHP = shieldHP;
		this.currentHullHP = hullHP;
		this.maxCapacitorAmount = capacitorAmount;
	}

	@Override
	public void endTurn() {
		this.capacitorAmount = PositiveInteger.of(Math.min(this.maxCapacitorAmount.value(),
				this.capacitorAmount.value() + this.capacitorRechargeRate.value()));
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.capacitorAmount.value() >= this.attackSubsystem.getCapacitorConsumption().value()) {
			this.capacitorAmount = PositiveInteger
					.of(this.capacitorAmount.value() - this.attackSubsystem.getCapacitorConsumption().value());
			return Optional
					.of(new AttackAction(this.attackSubsystem.attack(target), this, target, this.attackSubsystem));
		}
		else {
			return Optional.empty();
		}
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction reducedAttack = this.defenciveSubsystem.reduceDamage(attack);
		int damage = reducedAttack.damage.value();
		if (this.currentShieldHP.value() > 0) {
			int shieldHpAfterTakenDamage = this.currentShieldHP.value() - damage;
			if (shieldHpAfterTakenDamage < 0) {
				damage = Math.abs(shieldHpAfterTakenDamage);
			}
			else {
				damage = 0;
			}
			this.currentShieldHP = PositiveInteger.of(Math.max(0, shieldHpAfterTakenDamage));
		}
		this.currentHullHP = PositiveInteger.of(Math.max(0, this.currentHullHP.value() - damage));
		if (this.currentHullHP.value() == 0) {
			return new AttackResult.Destroyed();
		}
		else {
			return new AttackResult.DamageRecived(reducedAttack.weapon, reducedAttack.damage, reducedAttack.target);
		}
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.capacitorAmount.value() >= this.defenciveSubsystem.getCapacitorConsumption().value()) {
			if (!this.currentHullHP.value().equals(this.hullHP.value())
					|| !this.currentShieldHP.value().equals(this.shieldHP.value())) {
				RegenerateAction regenerateAction = this.defenciveSubsystem.regenerate();
				int hpToRecover = this.hullHP.value() - this.currentHullHP.value();
				int shieldToRecover = this.shieldHP.value() - this.currentShieldHP.value();
				PositiveInteger shieldRegeneratedValue = PositiveInteger
						.of(Math.min(regenerateAction.shieldHPRegenerated.value(), shieldToRecover));
				PositiveInteger hullRegeneratedValue = PositiveInteger
						.of(Math.min(regenerateAction.hullHPRegenerated.value(), hpToRecover));

				RegenerateAction recoveredAction = new RegenerateAction(shieldRegeneratedValue, hullRegeneratedValue);
				this.currentShieldHP = PositiveInteger
						.of(this.currentShieldHP.value() + shieldRegeneratedValue.value());
				this.currentHullHP = PositiveInteger.of(this.currentHullHP.value() + hullRegeneratedValue.value());
				this.capacitorAmount = PositiveInteger
						.of(this.capacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
				return Optional.of(recoveredAction);
			}
			else {
				this.capacitorAmount = PositiveInteger.of(Math.max(0,
						this.capacitorAmount.value() - this.defenciveSubsystem.getCapacitorConsumption().value()));
				return Optional.of(new RegenerateAction(PositiveInteger.of(0), PositiveInteger.of(0)));
			}
		}
		else {
			return Optional.empty();
		}
	}

}
