package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private final String name;

	private final PositiveInteger shieldHP;

	private final PositiveInteger hullHP;

	private final PositiveInteger powergridOutput;

	private final PositiveInteger capacitorAmount;

	private final PositiveInteger capacitorRechargeRate;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private Optional<AttackSubsystem> attackSubsystem;

	private Optional<DefenciveSubsystem> defenciveSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {

		if (name.trim().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		else {
			return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate,
					speed, size);
		}
	}

	private DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger powergridOutput,
			PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.powergridOutput = powergridOutput;
		this.capacitorAmount = capacitorAmount;
		this.capacitorRechargeRate = capacitorRechargeRate;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = Optional.empty();
		this.defenciveSubsystem = Optional.empty();
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.attackSubsystem = Optional.empty();
		}
		else {
			int powergridOutputValue = this.powergridOutput.value() - subsystem.getPowerGridConsumption().value();
			if (this.defenciveSubsystem.isPresent()) {
				powergridOutputValue = powergridOutputValue
						- this.defenciveSubsystem.get().getPowerGridConsumption().value();
			}
			if (powergridOutputValue >= 0) {
				this.attackSubsystem = Optional.of(subsystem);
			}
			else {
				throw new InsufficientPowergridException(Math.abs(powergridOutputValue));
			}
		}
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defenciveSubsystem = Optional.empty();
		}
		else {
			int powergridOutputValue = this.powergridOutput.value() - subsystem.getPowerGridConsumption().value();
			if (this.attackSubsystem.isPresent()) {
				powergridOutputValue = powergridOutputValue
						- this.attackSubsystem.get().getPowerGridConsumption().value();
			}
			if (powergridOutputValue >= 0) {
				this.defenciveSubsystem = Optional.of(subsystem);
			}
			else {
				throw new InsufficientPowergridException(Math.abs(powergridOutputValue));
			}
		}
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.attackSubsystem.isEmpty() && this.defenciveSubsystem.isEmpty()) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		if (this.attackSubsystem.isEmpty()) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		if (this.defenciveSubsystem.isEmpty()) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.capacitorAmount,
				this.capacitorRechargeRate, this.speed, this.size, this.attackSubsystem.get(),
				this.defenciveSubsystem.get());
	}

}
