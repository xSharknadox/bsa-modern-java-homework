package com.binary_studio.tree_max_depth;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null || rootDepartment.subDepartments == null) {
			return 0;
		}
		else {
			List<Department> subDepartments = rootDepartment.subDepartments.stream().filter(Objects::nonNull)
					.collect(Collectors.toList());
			int depth = 1;
			while (subDepartments.size() != 0) {
				depth++;
				List<Department> departmentsOfLevel = new ArrayList<>();
				for (Department department : subDepartments) {
					departmentsOfLevel.addAll(
							department.subDepartments.stream().filter(Objects::nonNull).collect(Collectors.toList()));
				}
				subDepartments = departmentsOfLevel;
			}
			return depth;
		}
	}

}
